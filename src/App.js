import React from 'react';
import MyRCForm from './containers/my-rc-form';
import RduexTest from './containers/rduex-test';
// import ReactRduexTest from './containers/react-rduex-test';
// import ReactRduexTest from './containers/react-rduex-test-2';
import ReactRduexTest from './containers/react-rduex-test-3';
import './css/app.css';

function App() {
  return (
    <div className="app">
      <header className="app-header">Test</header>
      <div className="app-body">
        {/* <MyRCForm /> */}
        {/* <RduexTest /> */}
        <ReactRduexTest />
      </div>
    </div>
  );
}

export default App;
