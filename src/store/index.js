import { createStore, applyMiddleware, combineReducers } from '../st-rduex'
import { logger, thunk } from '../middlewares';
import { counterReducer, ageReducer } from './reducers';

const store = createStore(combineReducers({
  count: counterReducer,
  age: ageReducer
}), applyMiddleware(thunk, logger));

export default store;