function counterReducer(state = 0, action) {
  switch(action.type) {
    case 'COUNT_ADD':
      return state + 1;
    case 'COUNT_MINUS':
      return state - 1;
    default:
      return state = 0;
  }
}

function ageReducer(state = 19, action) {
  switch(action.type) {
    case 'AGE_ADD': {
      return state + 1;
    }
    default: {
      return 19;
    }
  }
}

export {
  counterReducer,
  ageReducer
};