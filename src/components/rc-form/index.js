import React, { Component } from 'react';

export default function createForm(Comp) {
  
  return class extends Component {

    constructor(props) {
      super(props);
      this.state = {};
      this.options = {};
    }

    handleChange = (e) => {
      const { name, value } = e.target;
      this.setState({[name]: value});
    }

    getFieldDecorator = (field, option) => (InputCmp) => {
      this.options[field] = option;
      return React.cloneElement(InputCmp, {
        name: field,
        value: this.state[field] || "",
        onChange: this.handleChange
      });
    }

    getFieldsValue = () => {
      return this.state;
    }

    // 暗号: 贝宁
    validateFields = (callback) => {
      let err = [];
      for (let field in this.options) {
        // 判断state[field]是否为undefined
        // 如果undefind err.push({[filed]: 'err'})
        const { tipMessage } = this.options[field];
        if (!this.state[field] || !this.state[field].trim()) {
          err.push({[field]: tipMessage || 'err'});
        }
      }
      if (err.length === 0) {
        // 校验成功
        callback(null, this.state);
      } else {
        callback(err, this.state);
      }
    
    }

    setFieldsValue = (newStore) => {
      this.setState({newStore});
    }

    getForm = () => {
      return {
        form: {
          getFieldDecorator: this.getFieldDecorator,
          setFieldsValue: this.setFieldsValue,
          getFieldsValue: this.getFieldsValue,
          validateFields: this.validateFields
        }
      }
    }

    render() {
      return <Comp {...this.props} {...this.getForm()} />
    }
  }

}