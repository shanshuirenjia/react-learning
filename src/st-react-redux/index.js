
import React, { useContext, useLayoutEffect } from 'react';
import { bindActionCreators } from '../st-rduex';

const Context = React.createContext();
export function Provider({store, children}){
  return (
    <Context.Provider value={store}>
      {children}
    </Context.Provider>
  )
}

export const connect = (
  mapStateToProps = state => state,
  mapDispatchToProps
) => WrappedComponent => props => {
  const store = useContext(Context);
  const { dispatch, getState } = store;
  console.log(getState());
  const stateProps = mapStateToProps(getState());
  console.log(stateProps);

  let dispatchProps = {
    dispatch
  };

  if (typeof mapDispatchToProps === "object") {
    dispatchProps = bindActionCreators(mapDispatchToProps, dispatch);
  } else if (typeof mapDispatchToProps === "function") {
    dispatchProps = mapDispatchToProps(dispatch);
  }

//函数组件中引起更新
const [ignored, forceUpdate] = React.useReducer(x => x + 1, 0); // eslint-disable-line

// _ _
useLayoutEffect(() => {
  const unsubscribe = store.subscribe(() => {
    // 执行组件更新 forceUpdate
    forceUpdate();
  });
  return () => {
    if (unsubscribe) {
      unsubscribe();
    }
  };
}, [store]);

  return <WrappedComponent {...props} {...stateProps} {...dispatchProps} />
};

const useStore = function() {
  const store = React.useContext(Context);
  return store; 
}

export function useSelector(selector) {
  const store = useStore();
  const { getState } = store;
  const currentState = selector(getState());

  //函数组件中引起更新
const [ignored, forceUpdate] = React.useReducer(x => x + 1, 0); // eslint-disable-line

// _ _
useLayoutEffect(() => {
  const unsubscribe = store.subscribe(() => {
    // 执行组件更新 forceUpdate
    forceUpdate();
  });
  return () => {
    if (unsubscribe) {
      unsubscribe();
    }
  };
}, [store]);

  return currentState;
}

export function useDispatch() {
  const store = useStore();
  return store.dispatch;
}