export default function enhance(...middlewares) {
  return (createStore) => {
    return (reducer) => {
      const store = createStore(reducer);
      let dispatch = store.dispatch;

      const midApi = {
        getState: store.getState,
        dispatch: (action) => dispatch(action) 
      }

      // 装饰器, 装饰midApi
      // eg:
      // function decorate(instance) {
      //   // 执行A
      //   instance.interface();
      //   // 执行B
      // }

      const middlewareChain = middlewares.map(middleware => middleware(midApi));

      // 顺序执行 中间键
      dispatch = compose(...middlewareChain)(store.dispatch);

      return {
        ...store,
        dispatch  // 返回增强的dispath
      }

    }
  }
}

function compose(...funcs) {
  if (funcs.length === 0) {
    return arg => arg;
  }
  if (funcs.length === 1) {
    return funcs[0];
  }
  return funcs.reduce((a, b) => (...args) => a(b(...args)));
}