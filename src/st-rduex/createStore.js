function createStore(reducer, enhancer) {

  let currentState;
  let currentListeners = [];
  function getState() {
    return currentState;
  }

  function dispatch(action) {
    currentState = reducer(currentState, action);
    currentListeners.forEach(listener => listener());
  }

  function subscribe(listener) {
    currentListeners.push(listener);

    // 取消订阅
    return () => {
      currentListeners = currentListeners.filter(item => item !== listener);
    }
  }

  if (enhancer) {
    return enhancer(createStore)(reducer);
  }

  // 初始化 store
  dispatch({type: 'shunqinag-abcd'});

  return {
    getState,
    dispatch,
    subscribe
  }
}

export default createStore;