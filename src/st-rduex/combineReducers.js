// 暗号: 多哥
export default function combineReducers(reducers) {

  // state 发生变化分为两种情况
  // state 内部属性的值发生了变化
  // state 内部属性的数量发生了变化
  return function combination(state = {}, action) {
    let nextState = {};
    // 执行reducer 判断是否为值发生了变化
    let hasChanged = false;
    for (let key in reducers) {
      const reducer = reducers[key];
      nextState[key] = reducer(state[key], action);
      hasChanged = hasChanged || nextState[key] !== state[key];
    }

    // 判断是否为数量发生了变化
    hasChanged = hasChanged || Object.keys(nextState).length !== Object.keys(state).length;

    // 发生变化, 返回新的state, 没有发生变化原来的state
    return hasChanged ? nextState : state;
  }
}