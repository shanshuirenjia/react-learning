import React from 'react';
import PropTypes from 'prop-types';
import { connect } from '../st-react-redux';

class ReactRduexTest extends React.Component {

  onAdd = () => {
    let { add } = this.props;
    add();
  }

  onMinus = () => {
    let { minus } = this.props;
    minus();
  }

  render() {

    console.log(this.props);

    return (
      <div>
        <div>{'count: '}{this.props.count}</div>
        <button onClick={this.onAdd}>加1</button>
        <button onClick={this.onMinus}>减1</button>
        <button onClick={this.onAsyncAdd}>异步加1</button>
      </div>
    );
  }
}

export default connect(
  // mapStateToProps
  ({count}) => {
    return {count: count}
  },
  {
    add: () => {return {type: 'COUNT_ADD'}},
    minus: () => {return {type: 'COUNT_MINUS'}},
  }
)(ReactRduexTest);
