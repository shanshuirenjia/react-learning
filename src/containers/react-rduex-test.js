import React from 'react';
import PropTypes from 'prop-types';
import { connect } from '../st-react-redux';

const propTypes = {

};

class ReactRduexTest extends React.Component {

  onAdd = () => {
    let { dispatch } = this.props;
    dispatch({type: 'COUNT_ADD'})
  }

  onMinus = () => {
    const { dispatch } = this.props;
    dispatch({type: 'COUNT_MINUS'})
  }

  render() {

    console.log(this.props);

    return (
      <div>
        <div>{'count: '}{this.props.count}</div>
        <button onClick={this.onAdd}>加1</button>
        <button onClick={this.onMinus}>减1</button>
        <button onClick={this.onAsyncAdd}>异步加1</button>
      </div>
    );
  }
}

ReactRduexTest.propTypes = propTypes;

export default connect(
  // mapStateToProps
  ({count}) => {
    return {count: count}
  }
)(ReactRduexTest);
