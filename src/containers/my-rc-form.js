import React from 'react';
import PropTypes from 'prop-types';
// import { createForm } from 'rc-form';
import Input from '../components/Input';
import createForm from '../components/rc-form';

const propTypes = {

};

const nameRules = {required: true, message: "请输入姓名！"};
const passworRules = {required: true, message: "请输入密码！"};

class MyRCForm extends React.Component {

  onSubmit = () => {
    const { getFieldsValue, validateFields } = this.props.form;
    // console.log("submit", getFieldsValue());

    // 错误先行
    validateFields((err, val) => {
      if (err) {
        alert("err", err);
      } else {
        alert("校验成功", val);
      }
    })
  }

  render() {
    console.log(this.props);
    const { getFieldDecorator } = this.props.form; 
    return (
      <div>
        {getFieldDecorator("username", {rules: [nameRules]})(<Input placeholder="Username" />)}
        {getFieldDecorator("password", {rules: [passworRules]})(<Input placeholder="Password" />)}
        <button onClick={this.onSubmit}>提交</button>
      </div>
    );
  }
}

MyRCForm.propTypes = propTypes;

export default createForm(MyRCForm);
