import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from '../st-react-redux';

export default function() {
  const count = useSelector(({count}) => count);
  const dispatch = useDispatch();

  const add = () => dispatch({type: 'COUNT_ADD'});
  
  // 缓存对象
  const minus = useCallback(() => {
    dispatch({type: 'COUNT_MINUS'});
  }, [])

  return (
    <div>
      <div>{'count: '}{count}</div>
      <button onClick={add}>加1</button>
      <button onClick={minus}>减1</button>
      <button>异步加1</button>
    </div>
  );
}

