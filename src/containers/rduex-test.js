import React from 'react';
import PropTypes from 'prop-types';
import store from '../store';

const propTypes = {

};

class RduexTest extends React.Component {

  componentDidMount() {
    this.unsubscribe = store.subscribe(() => {
      this.forceUpdate(); // 强制刷新
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onAdd = () => {
    store.dispatch({type: 'COUNT_ADD'});
  }

  onAsyncAdd = () => {
    // 模拟下异步数据请求
    store.dispatch((dispatch, getState) => {
      setTimeout(() => {
        console.log("getState", getState());
        dispatch({type: "COUNT_ADD"});
      }, 1000);
    });
  }

  onMinus = () => {
    store.dispatch({type: 'COUNT_MINUS'});
  }

  onAddAge = () => {
    store.dispatch({type: 'AGE_ADD'})
  }

  render() {
    return (
      <div>
        <div>{'count: '}{store.getState().count}</div>
        <div>{'age: '}{store.getState().age}</div>
        <button onClick={this.onAdd}>加1</button>
        <button onClick={this.onAsyncAdd}>异步加1</button>
        <button onClick={this.onMinus}>减1</button>
        <button onClick={this.onAddAge}>年龄加1</button>
      </div>
    );
  }
}

RduexTest.propTypes = propTypes;

export default RduexTest;
